#!/usr/bin/env sh
#Pour sécuriser les communications entre vos API Spring Boot et Kafka en utilisant TLSv1.3, vous devez suivre les étapes suivantes :

#    Générer un certificat CA (Certificate Authority).
echo "Generate Certs"
mkdir certs
echo "some_secured_passw0rd" > ./certs/cert_creds

#    Générer un certificat CA (Certificate Authority) :
#        Créez un certificat CA qui sera utilisé pour signer les certificats de vos serveurs Kafka et de vos clients Spring Boot.
#
openssl req -new -x509 -keyout ca.key -out ca.crt -days 365 -subj "/CN=Kafka-CA" -passout pass:some_secured_passw0rd

#Créer des certificats pour les serveurs Kafka :
#
#    Pour chaque serveur Kafka, générez une clé privée et une CSR (Certificate Signing Request).
#
openssl req -new -newkey rsa:2048 -keyout kafka.server.key -out kafka.server.csr -days 365 -subj "/CN=kafka-server" -passout pass:some_secured_passw0rd

#    Utilisez le certificat CA pour signer la CSR du serveur Kafka.
#
openssl x509 -req -in kafka.server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out kafka.server.crt -days 365 -extensions v3_req -passin pass:some_secured_passw0rd

#Créer des certificats pour les clients Spring Boot :
#
#    Pour chaque client (c'est-à-dire vos applications Spring Boot), générez une clé privée et une CSR.
openssl req -new -newkey rsa:2048 -keyout client.key -out client.csr -days 365 -subj "/CN=spring-client" -passout pass:some_secured_passw0rd

#    Utilisez le certificat CA pour signer la CSR du client.
#
openssl x509 -req -in client.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out client.crt -days 365 -extensions v3_req -passin pass:some_secured_passw0rd

#    Créez les keystores et truststores nécessaires pour KAFKA en utilisant keytool.
#
keytool -keystore ./certs/kafka.server.keystore.jks -alias kafka-server -keyalg RSA -genkey -dname "CN=kafka-server" -validity 365 -storepass some_secured_passw0rd -keypass some_secured_passw0rd
keytool -keystore ./certs/kafka.server.truststore.jks -alias CARoot -import -file ca.crt -storepass some_secured_passw0rd -noprompt

#    Créez les keystores et truststores nécessaires pour les clients SPRING.
#
keytool -keystore ./certs/client.keystore.jks -alias spring-client -keyalg RSA -genkey -dname "CN=spring-client" -validity 365 -storepass some_secured_passw0rd -keypass some_secured_passw0rd
keytool -keystore ./certs/client.truststore.jks -alias CARoot -import -file ca.crt -storepass some_secured_passw0rd -noprompt

#Configurer Kafka pour utiliser TLS :
# Modifiez l'environnement de configuration docker compose pour ajouter les configurations nécessaires pour activer TLS.
#
#services:
#  kafka:
#    env_file:
#      - .env
#    environment:
#      KAFKA_LISTENERS: SSL://:29092
#      KAFKA_ADVERTISED_LISTENERS: SSL://:29092
#      KAFKA_SECURITY_INTER_BROKER_PROTOCOL: SSL
#      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: SSL:SSL
#      KAFKA_SSL_KEYSTORE_LOCATION: kafka.server.keystore.jks
#      KAFKA_SSL_KEY_PASSWORD: some_secured_passw0rd
#      KAFKA_SSL_TRUSTSTORE_LOCATION: kafka.server.truststore.jks
#      KAFKA_SSL_TRUSTSTORE_PASSWORD: truststore_password.txt
#      KAFKA_SSL_PROTOCOL: TLSv1.3

# Configurez les clients Spring Boot pour utiliser TLS :
#
#services:
#  spring:
#    env_file:
#      - .env
#    environment:
#      SPRING_KAFKA_SSL_KEY_STORE_LOCATION: /path/to/client.keystore.jks
#      SPRING_KAFKA_SSL_KEY_STORE_PASSWORD: some_secured_passw0rd
#      SPRING_KAFKA_SSL_KEY_PASSWORD: some_secured_passw0rd
#      SPRING_KAFKA_SSL_TRUST_STORE_LOCATION: /path/to/client.truststore.jks
#      SPRING_KAFKA_SSL_TRUST_STORE_PASSWORD: some_secured_passw0rd
#      SPRING_KAFKA_SSL_PROTOCOL: TLSv1.3

#spring / application.properties
#
#listeners=SSL://your.kafka.broker:9093
#ssl.keystore.location=/path/to/kafka.server.keystore.jks
#ssl.keystore.password=some_secured_passw0rd
#ssl.key.password=some_secured_passw0rd
#ssl.truststore.location=/path/to/kafka.server.truststore.jks
#ssl.truststore.password=some_secured_passw0rd
#ssl.protocol=TLSv1.3


#Configurer les clients Spring Boot pour utiliser TLS :
#
#    Ajoutez les configurations nécessaires dans les fichiers application.properties ou application.yml de vos applications Spring Boot pour utiliser les keystores et truststores.

#spring / application.properties

#spring.kafka.ssl.key-store-location=classpath:client.keystore.jks
#spring.kafka.ssl.key-store-password=some_secured_passw0rd
#spring.kafka.ssl.key-password=some_secured_passw0rd
#spring.kafka.ssl.trust-store-location=classpath:client.truststore.jks
#spring.kafka.ssl.trust-store-password=some_secured_passw0rd
#spring.kafka.ssl.protocol=TLSv1.3


#Tester la configuration :
#
#    Redémarrez vos serveurs Kafka et vos applications Spring Boot.
#    Assurez-vous que les communications entre les applications Spring Boot et Kafka utilisent TLSv1.3 en vérifiant les logs.

# wait user input to exit
read -p "Press enter to exit"