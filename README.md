# deploy



## Description projet

Ce projet a pour seul but de donner les instructions et scripts pour déployer facilement notre application.

## Prérequis

- OpenSSH latest
- Docker latest
- Git latest

## Génération des fichiers .env
```shell
./generate-app_envs.sh
```

## Lancer l'intégralité de l'application :
```shell
docker-compose -f compose.globalTest.yml up -d
```


## Lancer l'application sécurisée

### Générer les certificats
```shell
./generate-certs.sh
```
### Lancer l'application
```shell
docker-compose -f compose.global-secured.yml
```