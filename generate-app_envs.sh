#!/usr/bin/env bash

# This file will create the necessary environment variables to secure the communications between your Spring Boot APIs and Kafka using TLSv1.3.
mkdir -p env

## env/.env.kafka
#echo "KAFKA_SSL_KEY_CREDENTIALS=some_secured_passw0rd" > env/.env.kafka
#echo "KAFKA_SSL_TRUSTSTORE_CREDENTIALS=some_secured_passw0rd" >> env/.env.kafka
#echo "KAFKA_SSL_KEYSTORE_CREDENTIALS=some_secured_passw0rd" >> env/.env.kafka
#echo "KAFKA_SSL_PROTOCOL=TLSv1.3" >> env/.env.kafka
#echo "KAFKA_SSL_KEYSTORE_LOCATION=certs/kafka.server.keystore.jks" >> env/.env.kafka
#echo "KAFKA_SSL_TRUSTSTORE_LOCATION=certs/kafka.server.truststore.jks" >> env/.env.kafka
#
#
#
## env/.env.spring-secured.global
#echo "SPRING_KAFKA_SSL_KEY_STORE_PASSWORD=some_secured_passw0rd" > env/.env.spring-secured.global
#echo "SPRING_KAFKA_SSL_KEY_PASSWORD=some_secured_passw0rd" >> env/.env.spring-secured.global
#echo "SPRING_KAFKA_SSL_TRUST_STORE_PASSWORD=some_secured_passw0rd" >> env/.env.spring-secured.global

# env/.env.db.orders
echo "POSTGRES_PASSWORD=postgres1" > env/.env.pg.orders
echo "POSTGRES_USER=postgres" >> env/.env.pg.orders
echo "POSTGRES_DB=postgres1" >> env/.env.pg.orders

# env/.env.app.orders
echo "SPRING_DATASOURCE_PASSWORD=postgres1" > env/.env.app.orders
echo "SPRING_DATASOURCE_USERNAME=postgres" >> env/.env.app.orders
echo "DB_NAME=postgres1" >> env/.env.app.orders

# env/.env.db.customers
echo "POSTGRES_PASSWORD=postgres2" > env/.env.pg.customers
echo "POSTGRES_USER=postgres" >> env/.env.pg.customers
echo "POSTGRES_DB=postgres2" >> env/.env.pg.customers

# env/.env.app.customers
echo "SPRING_DATASOURCE_PASSWORD=postgres2" > env/.env.app.customers
echo "SPRING_DATASOURCE_USERNAME=postgres" >> env/.env.app.customers
echo "DB_NAME=postgres2" >> env/.env.app.customers

# env/.env.db.products
echo "POSTGRES_PASSWORD=postgres3" > env/.env.pg.products
echo "POSTGRES_USER=postgres" >> env/.env.pg.products
echo "POSTGRES_DB=postgres3" >> env/.env.pg.products

# env/.env.app.products
echo "SPRING_DATASOURCE_PASSWORD=postgres3" > env/.env.app.products
echo "SPRING_DATASOURCE_USERNAME=postgres" >> env/.env.app.products
echo "DB_NAME=postgres3" >> env/.env.app.products


